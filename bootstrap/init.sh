#!/usr/bin/env bash

apt-get update
apt install -y gnome-session gdm3
apt install -y gnome-terminal firefox
apt install -y neovim
apt install -y python3 python3-pip
apt install -y zsh zsh-antigen

useradd -m -s /bin/zsh -U z1lq
chown -R z1lq:z1lq /home/z1lq
echo "%z1lq ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/z1lq

mkdir -p /home/z1lq/tools/temp
cd /home/z1lq/tools/temp
wget https://github.com/OWASP/Amass/releases/download/v3.13.4/amass_linux_amd64.zip
unzip -d ../ amass_linux_amd64.zip

wget https://github.com/tomnomnom/httprobe/releases/download/v0.1.2/httprobe-linux-amd64-0.1.2.tgz
tar -xvf httprobe-linux-amd64-0.1.2.tgz -C ../

pip3 install dnsgen

wget https://github.com/michenriksen/aquatone/releases/download/v1.7.0/aquatone_linux_amd64_1.7.0.zip
unzip -d ../ aquatone_linux_amd64_1.7.0.zip

wget https://github.com/ffuf/ffuf/releases/download/v1.3.1/ffuf_1.3.1_linux_amd64.tar.gz
gzip -dc ffuf_1.3.1_linux_amd64.tar.gz | tar -xf - -C ../

wget -c https://github.com/danielmiessler/SecLists/archive/master.zip -O SecList.zip
unzip -qod ../ SecList.zip

cd ..
rm -r temp

